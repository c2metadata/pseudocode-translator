package us.mtna.pseudocode.tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.pseudocode.translator.manager.PseudocodeServiceImpl;

public class ServiceTest {
	//TODO create tests around this https://gitlab.com/c2metadata/SDTL-translation-libraries/-/blob/master/SDTL-pseudocode_library/PseudocodeLibraryTexts.txt

	private String functionLibraryUrl;;
	private String pseudocodeUrl;
	private PseudocodeServiceImpl service;
	
	//here are some example strings to use with the toText() method
	private String load =  "{   \"$type\": \"Load\",\r\n   \"command\": \"load\",\r\n  \"sourceInformation\": {\r\n                \"lineNumberStart\": 3,\r\n                \"lineNumberEnd\": 3,\r\n                \"sourceStartIndex\": 66,\r\n                \"sourceStopIndex\": 89,\r\n                \"originalSourceText\": \"GET  FILE='D:path.sav'.\"\r\n            },\r\n            \"fileName\": \"'D:path.sav'\"\r\n        }";
	private String noTransformOp = "  {\r\n        \"$type\": \"NoTransformOp\",\r\n        \"command\": \"NoTransformOp\",\r\n        \"sourceInformation\": \r\n        {\r\n            \"$type\": \"SourceInformation\",\r\n            \"lineNumberStart\": 1,\r\n            \"lineNumberEnd\": 1,\r\n            \"sourceStartIndex\": 1,\r\n            \"sourceStopIndex\": 19,\r\n            \"originalSourceText\": \"import pandas as pd\" \r\n        }\r\n\r\n    },\r\n";
	private String compute = "{\r\n        \"$type\": \"Compute\",\r\n        \"command\": \"Compute\",\r\n        \"consumesDataframe\": [{\"dataframeName\": \"df\", \"variableInventory\": [\"A\", \"B\"]}],\r\n        \"expression\": \r\n        {\r\n            \"$type\": \"NumericConstantExpression\",\r\n            \"numericType\": \"Integer\",\r\n            \"value\": \"3\"\r\n        },\r\n        \"producesDataframe\": [{\"dataframeName\": \"df\", \"variableInventory\": [\"A\", \"B\"]}],\r\n        \"variable\": \r\n        {\r\n            \"$type\": \"VariableSymbolExpression\",\r\n            \"variableName\": \"A\"\r\n        },\r\n        \"sourceInformation\": \r\n        {\r\n            \"$type\": \"SourceInformation\",\r\n            \"lineNumberStart\": 12,\r\n            \"lineNumberEnd\": 12,\r\n            \"sourceStartIndex\": 98,\r\n            \"sourceStopIndex\": 108,\r\n            \"originalSourceText\": \"df[\\\"A\\\"] = 3\" \r\n        }\r\n    },\r\n";
	
	@Before
	public void setUp(){
		//set the urls to the relevant libraries and initialize the service component before we run the tests.
		functionLibraryUrl = "https://gitlab.com/c2metadata/SDTL-translation-libraries/raw/master/SDTL_Function_Library/SDTL_Function_Library.json?inline=false";
		pseudocodeUrl = "https://gitlab.com/c2metadata/SDTL-translation-libraries/raw/master/SDTL-pseudocode_library/Pseudocode_Library.json?inline=false";
		service = new PseudocodeServiceImpl(functionLibraryUrl, pseudocodeUrl);
		
	}
	
	////////// TESTING THE TOTEXT() METHOD ////////////
	
	//this test tests a single (escaped) sdtl command and the .toText() method. 
	@Test
	public void testLoadCommand() throws JsonParseException, JsonMappingException, IOException{
		//I escaped this text, but you could read it from a file.
		ObjectMapper mapper = new ObjectMapper();
		HashMap commandJson = mapper.readValue(load, HashMap.class);
		String output = service.toText(commandJson);
		System.out.println(output);
		Assert.assertTrue(output.equals("Load file 'D:path.sav'"));
	}
	
	
	////////// TESTING THE GENERATE() METHOD ////////////

	
	//this test reads a whole Program object from a file 
	//It seems like the generate method only works with the whole Program object and not a single command
	@Test
	public void test1FunctionCompute() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/1function_compute.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to (var01/var02) .";
		//remove newlines and whitespace before comparing
		Assert.assertEquals(expected, output.trim().replace("\n", ""));
	}
	
	@Test
	public void testAdditionFunction() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/addition_function.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to (var01 +var02) .";
		//remove newlines and whitespace before comparing
		Assert.assertEquals(expected, output.trim().replace("\n", ""));
	}
	
	@Test
	public void testDivideAdd() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/Divide_Add.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to ((var01/var02) +3) .";
		//remove newlines and whitespace before comparing
		Assert.assertEquals(expected, output.trim().replace("\n", ""));
	}
	
	@Test
	public void testLnFunction() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/LN_function.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to natural log of var01 .";
		//remove newlines and whitespace before comparing
		Assert.assertEquals(expected, output.trim().replace("\n", ""));
	}
	
	@Test
	public void testLnDivisionFunction() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/LN_division_function.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to natural log of (var01/var02) .";
		//remove newlines and whitespace before comparing
		Assert.assertEquals(expected, output.trim().replace("\n", ""));
	}
	//the below ones don't work
	
	//compute var11 = trunc(var01/var02).
	//the second exp2 isn't getting replaced with zero because there isn't a second argument of zero anywhere to replace it with 
	@Test
	public void test2FunctionCompute() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/2function_compute.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to (truncate (var01/var02) to a multiple of 0) .";
		//remove newlines and whitespace before comparing
		System.out.println("actual: "+output.trim().replace("\n", ""));
		System.out.println("expected: "+expected);
		Assert.assertEquals(expected, output.trim().replace("\n", ""));

	}
	//this revision works (function argument added) but needs to have the parentheses removed.
	//it looks like the parentheses are in the funciton library syntax
	@Test
	public void test2FunctionComputeREvised() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/2function_compute_revised.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to (truncate (var01/var02) to a multiple of 0) .";
		//remove newlines and whitespace before comparing
		System.out.println("actual: "+output.trim().replace("\n", ""));
		System.out.println("expected: "+expected);
		Assert.assertEquals(expected, output.trim().replace("\n", ""));

	}
	@Test
	public void test3FunctionCompute() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/3function_compute.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to ((truncate (var01/var02) to a multiple of 0) + 3) .";
		System.out.println("actual: "+output.trim().replace("\n", ""));
		System.out.println("expected: "+expected);
		//remove newlines and whitespace before comparing
		Assert.assertEquals(expected, output.trim().replace("\n", ""));

	}
	@Test
	public void testTruncFunction() throws JsonParseException, JsonMappingException, IOException{
		 byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/Trunc_function.json"));
		 String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to (truncate var01 to a multiple of 1) .";
		//remove newlines and whitespace before comparing
		System.out.println("actual: "+output.trim().replace("\n", ""));
		System.out.println("expected: "+expected);
		Assert.assertEquals(expected, output.trim().replace("\n", ""));

	}
	//this one the sdtl seems wrong? there is a value argument listed, but the value is zero and we are expecting one.
	@Test
	public void testTruncFunctionExp2() throws JsonParseException, JsonMappingException, IOException{
		byte[] encoded = Files.readAllBytes(Paths.get("src/test/resources/Pseudocode_test_files/Trunc_function_EXP2.json"));
		String command = new String(encoded, "UTF-8");
		String output = service.generate(command);
		String expected = "Set var11 to (truncate var01 to a multiple of 1) .";
		//remove newlines and whitespace before comparing
		System.out.println("actual: "+output.trim().replace("\n", ""));
		System.out.println("expected: "+expected);
		Assert.assertEquals(expected, output.trim().replace("\n", ""));

	}
	
	
}

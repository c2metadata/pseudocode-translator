SDTL files that work:

1function_compute.json
	"Set var11 to (var01/var02) ."
	
addition_function.json
	"Set var11 to (var01 +var02) ."
	
Divide_Add.json	
	"Set var11 to ((var01/var02) +3) ."

LN_function.json
    "Set var11 to natural log of var01 ."
	
LN_division_function.json	
	"Set var11 to natural log of (var01/var02) ."
-------------------------------------------------

SDTL files that do NOT work:	

2function_compute.json
	"Set var11 to (truncate (var01/var02) to a multiple of 0) ."

3function_compute.json
	"Set var11 to ((truncate (var01/var02) to a multiple of 0) + 3) ."

Trunc_function.json
	"Set var11 to (truncate var01 to a multiple of 0) ."

Trunc_function_EXP2.json
	"Set var11 to (truncate var01 to a multiple of 1) ."
	

	

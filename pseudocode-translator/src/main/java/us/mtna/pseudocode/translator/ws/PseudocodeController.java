package us.mtna.pseudocode.translator.ws;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.pseudocode.translator.manager.PseudocodeServiceImpl;

@RequestMapping("/pseudocode")
@RestController
public class PseudocodeController {

	private static final Logger LOG = Logger.getLogger(PseudocodeController.class);
	private PseudocodeServiceImpl pseudocodeServiceImpl;

	public PseudocodeController(PseudocodeServiceImpl pseudocodeServiceImpl) {
		this.pseudocodeServiceImpl = pseudocodeServiceImpl;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView pseudoCodeTestView() {
		LOG.info("Executing parseSdtl.");
		ModelAndView mv = new ModelAndView("readable");
		return mv;
	}

	@RequestMapping(value = "/text", method = RequestMethod.POST)
	public @ResponseBody String returnPseudoCodeAsText(@RequestParam(required = false) String sdtlString,
			HttpServletRequest req, HttpServletResponse resp) {
		LOG.info("Executing parseSdtl.");

		try {
			return pseudocodeServiceImpl.generate(sdtlString);

		} catch (Exception e) {
			throw new RuntimeException(e);
//			LOG.error(e);

		}
//		return "Unable to Parse Pseudocode";
	}

	@RequestMapping(value = "/command/text", method = RequestMethod.POST)
	public @ResponseBody String returnPseudoCodeAsTextforCommand(@RequestParam(value = "command", required = false) String command,
			HttpServletRequest req, HttpServletResponse resp) {

		try {
			LOG.info("Executing parseSdtl.");
			ObjectMapper mapper = new ObjectMapper();
			HashMap commandJson = mapper.readValue(command, HashMap.class);
			return pseudocodeServiceImpl.toText(commandJson);

		} catch (Exception e) {
			throw new RuntimeException(e);
//			LOG.error(e);

		}
//		return "Unable to Parse Pseudocode";
	}
	


	@RequestMapping(value = "/textview", method = RequestMethod.POST)
	public String parsePsedoCode(@RequestParam(required = false) MultipartFile sdtlFile, HttpServletRequest req,
			HttpServletResponse resp) {
		LOG.info("Executing parseSdtl.");
		try {
			String sdtlAsString = new String(sdtlFile.getBytes());
			String humanText = pseudocodeServiceImpl.generate(sdtlAsString);

			return humanText;
		} catch (Exception e) {
			LOG.error(e.getMessage(),e);
		}
		return "Unable to parse SDTL into human-readable text.";
	}

}

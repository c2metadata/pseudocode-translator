package us.mtna.pseudocode.translator.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.parsing.ParseState.Entry;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("rawtypes")
public class PseudocodeServiceImpl {

	private static final Logger LOG = Logger.getLogger(PseudocodeServiceImpl.class);

	private ArrayList functionList = new ArrayList();
	private HashMap pseudoCodeLib = null;
	private ArrayList commands = null;
	private String sdtl = null;
	private String functionLibPath;
	private String pseudoCodeLibPath;

	public PseudocodeServiceImpl(String functionLibraryUrl, String pseudocodeUrl) {
		this.functionLibPath = functionLibraryUrl;
		this.pseudoCodeLibPath = pseudocodeUrl;
		init();
	}

	// @PostConstruct
	private void init() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getInterceptors().add(new ClientHttpRequestInterceptor() {
				@Override
				public ClientHttpResponse intercept(HttpRequest request, byte[] body,
						ClientHttpRequestExecution execution) throws IOException {
					request.getHeaders().set("User-Agent",
							"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");// Set
																																					// the
																																					// header
																																					// for
																																					// each
																																					// request
					return execution.execute(request, body);
				}
			});

			String jsonList = "[" + restTemplate.getForObject(functionLibPath, String.class) + "]";
			List functionLibList = mapper.readValue(jsonList, List.class);
			for (int i = 0; i < functionLibList.size(); i++) {
				functionList.addAll(((HashMap) functionLibList.get(i)).values());
			}
			String psuedoList = restTemplate.getForObject(pseudoCodeLibPath, String.class);
			pseudoCodeLib = mapper.readValue(psuedoList, HashMap.class);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	private ArrayList getCommands(String sdtl) {

		try {

			ObjectMapper mapper = new ObjectMapper();
			HashMap sdtlJson = mapper.readValue(sdtl, HashMap.class);
			if (sdtlJson.get("commands") != null) {
				return (ArrayList) sdtlJson.get("commands");
			} else if (sdtlJson.get("Commands") != null) {
				return (ArrayList) sdtlJson.get("Commands");
			} else {
				return null;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	private String getSourceLanguage(String sdtl) {

		try {

			ObjectMapper mapper = new ObjectMapper();
			HashMap sdtlJson = mapper.readValue(sdtl, HashMap.class);
			if (sdtlJson.get("SourceLanguage") != null) {
				return sdtlJson.get("SourceLanguage").toString();
			} else if (sdtlJson.get("sourceLanguage") != null) {
				return sdtlJson.get("sourceLanguage").toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	public String generate(String sdtlString) {
		LOG.info("Executing generate from String input.");
		this.init();
		commands = this.getCommands(sdtlString);
		this.sdtl = sdtlString;
		String sourceLanguage = "software " + this.getSourceLanguage(sdtlString);
		LOG.info("Executing generate from Program input.");
		StringBuilder output = new StringBuilder();
		for (int i = 0; i < commands.size(); i++) {
			HashMap command = (HashMap) commands.get(i);
			String out = toText(command, sourceLanguage);
			output.append(out + " .\r\n");
		}
		return output.toString();
	}

	public String toText(HashMap commandMap, String... sourceLanguage) {
		String output = "";
		try {

			List<String> keysList = new ArrayList<>(commandMap.keySet());
			String command = (String) commandMap.get("command");
			if (StringUtils.isBlank(command)) {
				command = (String) commandMap.get("$type");
			}
			HashMap psCodeMap = getPseudocodeMap(command);
			ArrayList paramters = (ArrayList) psCodeMap.get("paramters");
			String outPutText = StringUtils.trimToEmpty((String) psCodeMap.get("BaseText"));
			if (sourceLanguage.length > 0) {
				outPutText = outPutText.replace("{sourceLanguage}", sourceLanguage[0]);
			}
			for (int j = 0; j < paramters.size(); j++) {
				HashMap paramMAp = (HashMap) paramters.get(j);
				String param = (String) paramMAp.get("PropertyName");
				String replaceParam = "{" + param + "}";
				if (param != null && !param.isEmpty()) {

					for (String key : keysList) {
						if (param.equalsIgnoreCase(key)) {
							String text = (String) paramMAp.get("text");
							String rqType = (String) paramMAp.get("reqType");
							if (text != null && !text.isEmpty())
								// outPutText+=text;

								if (StringUtils.isNotBlank(rqType)) {
									if (rqType.equals("boolean")) {
										if (commandMap.get("newRow") != null
												&& ((boolean) commandMap.get("newRow")) == false) {
											outPutText = "";
										}

										break;
									}
									List innerCommsList = (ArrayList) commandMap.get(key);

									for (int ijk = 0; ijk < innerCommsList.size(); ijk++) {

										HashMap innerCommandMap = (HashMap) innerCommsList.get(ijk);
										innerCommandMap.put("command", rqType);
										innerCommandMap.putAll((HashMap) innerCommsList.get(ijk));
										if (StringUtils.isNotBlank(text)) {
											if (text.contains(param)) {
												outPutText += text.replace(replaceParam, toText(innerCommandMap));
											} else {
												// need to verify this
												outPutText += " " + toText(innerCommandMap);
											}
										} else {
											// outPutText+=text;
											if (outPutText.contains(param))
												outPutText = outPutText.replace(replaceParam, toText(innerCommandMap));
											else
												outPutText += ".\r\n" + toText(innerCommandMap);
										}

									}

									break;
								}
							Object obj = commandMap.get(key);
							if (obj instanceof String) {
								if (StringUtils.isNotBlank(text)) {
									if (text.contains(param)) {
										outPutText += " " + text.replace(replaceParam, obj.toString());
									} else {
										outPutText += " " + obj.toString();
									}
								} else {
									if (outPutText.contains(param))
										outPutText = outPutText.replace(replaceParam, obj.toString());
									else
										outPutText += " " + obj.toString();
								}
							} else if (obj instanceof Map) {
								HashMap deepParamMAp = (HashMap) obj;
								if ("FunctionCallExpression".equalsIgnoreCase((String) deepParamMAp.get("$type"))) {
									if (StringUtils.isNotBlank(text)) {
										if (text.contains(param)) {
											outPutText += " "
													+ text.replace(replaceParam, retrieveParamOutput(deepParamMAp));
										} else {
											outPutText += " " + retrieveParamOutput(deepParamMAp);
										}
									} else {
										if (outPutText.contains(param))
											outPutText = outPutText.replace(replaceParam,
													retrieveParamOutput(deepParamMAp));
										else {
											outPutText += " " + retrieveParamOutput(deepParamMAp);
										}
									}
								} else {
									if (StringUtils.isNotBlank(text)) {
										if (text.contains(param)) {
											outPutText += " " + text.replace(replaceParam, toText(deepParamMAp));
										} else {
											outPutText += toText(deepParamMAp);
										}
									} else {
										if (outPutText.contains(param))
											outPutText = outPutText.replace(replaceParam, toText(deepParamMAp));
										else
											outPutText += toText(deepParamMAp);
									}

									// outPutText=
									// outPutText.replace(param,toText(deepParamMAp));

								}
							} else if (obj instanceof List) {
								List innerCommsList = (ArrayList) obj;
								for (int ijk = 0; ijk < innerCommsList.size(); ijk++) {

									HashMap innerCommandMap = (HashMap) innerCommsList.get(ijk);
									innerCommandMap.putAll((HashMap) innerCommsList.get(ijk));
									if (!(innerCommandMap.containsKey("command")
											|| innerCommandMap.containsKey("$type")))
										innerCommandMap.put("command", rqType);
									if (StringUtils.isNotBlank(text)) {
										if (text.contains(param)) {
											outPutText += ".\r\t" + text.replace(replaceParam, toText(innerCommandMap));
										} else {
											outPutText += ".\r\t" + toText(innerCommandMap);
										}
									} else {
										if (outPutText.contains(param))
											outPutText = outPutText.replace(replaceParam, toText(innerCommandMap));
										else
											outPutText += ".\r\n" + toText(innerCommandMap);
									}
								}
							}

							break;
						}
					}

				}
			}
			output += outPutText;
			output = output.replaceAll("\\{", "");
			output = output.replaceAll("\\}", "");
			LOG.error("outputGenereated " + output);
			LOG.error(commandMap);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return output;
	}

	private String retrievOutput(Object obj, String outPutText, String param) {
		String outPut = "";
		try {
			if (obj instanceof String) {

				outPutText = outPutText.replace(param, obj.toString());
			} else if (obj instanceof Map) {
				HashMap paramMAp = (HashMap) obj;
				if ("FunctionCallExpression".equalsIgnoreCase((String) paramMAp.get("$type"))) {
					outPut += outPutText.replace(param, retrieveParamOutput(paramMAp));
				} else {
					outPut += outPutText.replace(param, toText(paramMAp));

				}
			}
			outPut += outPutText;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return outPut;
	}

	private String retrieveParamOutput(HashMap functionMap) {
		String outPut = "";
		String function = (String) functionMap.get("function");
		HashMap functionLibMap = getFunctionLibeMap(function);

		if (functionLibMap != null) {
			String pseduoText = (String) functionLibMap.get("Pseudocode");
			int argsCount = Integer.parseInt((String) functionLibMap.get("n_arguments"));
			ArrayList arguments = (ArrayList) functionMap.get("arguments");
			ArrayList params = (ArrayList) functionLibMap.get("parameters");
			int x = 0;
			for (int i = 0; i < argsCount; i++) {
				String paramName = (String) (((HashMap) params.get(i)).get("param"));
				HashMap listMap = null;
				if (arguments.size() <= i ) {
					listMap = new HashMap<>();
					listMap.put("value", ((HashMap) params.get(i)).get("required"));
				} else {
					listMap = (HashMap) ((HashMap) arguments.get(i)).get("argumentValue");

				}
				// uses param order unless name is found, then overwrite the
				// listmap value
				if (arguments.size() > i && ((HashMap) arguments.get(i)).get("argumentName") != null) {
					for (int j = 0; j < argsCount; j++) {
						if (((HashMap) arguments.get(j)).get("argumentName").equals(paramName)) {
							listMap = (HashMap) ((HashMap) arguments.get(j)).get("argumentValue");
						}
					}
				}
				String argType = (String) listMap.get("$type");
				if ("FunctionCallExpression".equalsIgnoreCase(argType)) {
					pseduoText = pseduoText.replace(paramName, retrieveParamOutput(listMap));
				} else if ("GroupedExpression".equalsIgnoreCase(argType)) {
					HashMap groupedMap = (HashMap) listMap.get("expression");
					pseduoText = pseduoText.replace(paramName, retrieveParamOutput(groupedMap));
				} 
				else {
					if (listMap.containsKey("variableName")) {
						pseduoText = pseduoText.replace(paramName, (String) listMap.get("variableName"));
					} else if (listMap.containsKey("value")) {
						pseduoText = pseduoText.replace(paramName, (String) listMap.get("value"));
					}else if (listMap.containsKey("variables")) {
						List<Map> vars =(ArrayList)listMap.get("variables");
						StringBuilder sb = new StringBuilder();
						for (Map entry : vars) {
							if(entry.containsKey("first")){
								sb.append(entry.get("first")).append(" to ");
							}
							if(entry.containsKey("last")){
								sb.append(entry.get("last"));
							}
							
						}
						pseduoText = pseduoText.replace(paramName, sb.toString());
					}
				}

			}
			// check if the second value (eXP2) has a default value and use that
			// as an arg if so
			if (argsCount > arguments.size()) {
				System.out.println("actual args: " + (String) functionLibMap.get("n_arguments"));
				System.out.println("map args: " + arguments);
				System.out.println("params: " + params);

			}
			outPut += pseduoText;
		}
		return outPut;
	}

	private HashMap getPseudocodeMap(String command) {
		if (command == null)
			command = "";

		ArrayList array = (ArrayList) pseudoCodeLib.get("PseudocodeLibrary");
		for (int i = 0; i < array.size(); i++) {
			HashMap obj = (HashMap) array.get(i);
			String sdtlNAme = (String) obj.get("SDTLname");

			if (command.equalsIgnoreCase(sdtlNAme)) {
				return obj;
			}
		}
		return null;
	}

	private HashMap getFunctionLibeMap(String function) {
		for (int i = 0; i < functionList.size(); i++) {
			for (int j = 0; j < ((ArrayList) functionList.get(i)).size(); j++) {

				HashMap obj = ((HashMap) ((ArrayList) functionList.get(i)).get(j));
				String sdtlNAme = (String) obj.get("SDTLname");
				if (function.equalsIgnoreCase(sdtlNAme)) {
					return obj;
				}
			}
		}
		return null;
	}

}
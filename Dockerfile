FROM tomcat:9.0.19-jre8-alpine

LABEL maintainer="C2Metadata"

COPY ./pseudocode-translator/target/pseudocode-translator.war /tmp
#COPY pseudocode-translator.war /tmp

RUN mkdir /usr/local/tomcat/webapps/pseudocode-translator \
    && unzip /tmp/pseudocode-translator.war -d /usr/local/tomcat/webapps/pseudocode-translator
